# randnumberfile

This program has been developed to simply create ascii random number files.

The options asked are

	Filename
	Elements in file
	Max size of each element

for example you could create a file with 1000 numbers (elements), ranging from 0 to 255 (max size)

current max size of elements and max size of each element is 2,147,483,647.

This program can be installed by downloading and extracting the source files, opening a terminal in the new directory and typing "make" to see the installation options.