# Application : randnumberfile
#	Description
# Author Raymond Thomson
#
#
#

all: help

CC=gcc
MAN=man/
SRC=src/
CFLAGS=-I.
LIBS=

%.o: %.c
	@echo 'Invoking: GCC C object files'
	$(CC) -c -o $($SRC)$@ $< $(CFLAGS)

randnumberfile: $(SRC)randnumberfile.o
	@echo 'Building and linking target: $@'
	$(CC) -o randnumberfile $(SRC)randnumberfile.o $(LIBS)

local: randnumberfile clean

clean:
	rm -f $(SRC)*.o 

install: randnumberfile clean
	cp randnumberfile /usr/local/bin/randnumberfile
	cp $(MAN)randnumberfile /usr/share/man/man1/randnumberfile.1
	gzip /usr/share/man/man1/randnumberfile.1
	rm -f randnumberfile
	@echo 'Installed. Enter randnumberfile to run or man randnumberfile for the manual page'

remove:
	rm -f randnumberfile

uninstall:
	rm -f /usr/local/bin/randnumberfile
	rm -f /usr/share/man/man1/randnumberfile.1.gz

help:
	@echo 'Make options for randnumberfile'
	@echo 'Local Folder make'
	@echo '    make local'
	@echo 'Local Folder uninstall'
	@echo '    make remove'
	@echo 'System Install'
	@echo '    sudo make install'
	@echo 'System Uninstall'
	@echo '    sudo make uninstall'

