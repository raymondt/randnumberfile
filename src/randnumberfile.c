/*
 *  Randfile.c
 *
 *  Created on: 17 Mar 2019
 *  Author: Ray Thomson
 *
 *  Ascii random number file generator
 *
 *	file name ( the name of the file to create )
 *	Number of elements ( the number of numbers created e.g. 8 numbers = ( 1,2,3,4,5,6,7,8 )
 *  Max size of number ( currently between 1 and max signed int = 2,147,483,647 )
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#define MY_MAX 2147483647

int main(int argc, char *argv[]) {

	printf("Random File ANumber Generator. V0.2\n\n");

	// progress meter variables
	int current = 0;
	int prog;

//	int i;
	int res;
	int arraysize;
	int maxsize;
	char filename[255];
	char numlen[255];

	// Initialise random number generator
	srand((unsigned) time(0));

	printf("Enter file name to create or overwrite (max 255 chars)\n");
	scanf("%s",filename);

	printf("Enter number of elements in the array (max %i)\n",MY_MAX);
	scanf("%i",&arraysize);

	if (arraysize > MY_MAX) arraysize = MY_MAX;

	printf("Enter max size of each element (max %i)\n",MY_MAX);
	scanf("%s",numlen);

	int slen = strlen(numlen);
	maxsize = atoi(numlen);
	double tot = ((double)slen+1)*arraysize;
	printf("Using the value %d for each element,\nthe created file size could be up to %e bytes long\n",maxsize ,tot);

	printf("Enter y to continue\n");
	scanf("%s",numlen);

	if (numlen[0] != 'y') exit(10);

	if (maxsize > MY_MAX) maxsize = MY_MAX;

	printf("Creating Random Array\n");
	printf("Filename %s\n",filename);
	printf("Elements %d\n",arraysize);
	printf("Maximum %d\n",maxsize);
	printf("EstFileSize %e\n",tot);

	FILE *myF = fopen(filename,"w");

	printf("0%%");
	fflush(stdout);
	current = 0;
	for (int p = 0; p < (arraysize-1); p++) {

		res = (rand()+1) % maxsize;
		if (res < 0) {
			res = -res;
		}
		fprintf(myF,"%i\n",res);

		float f = (float)p / arraysize;
		prog = (int)(f*100);

		if (prog != current) {
			printf("\b\b\b");
			printf("%i%%",prog);
			current = prog;
		}

	}
	res = (rand()+1) % maxsize;
		if (res < 0) {
			res = -res;
		}
	fprintf(myF,"%d",res);

	fclose(myF);

	printf("\nDone 100%%\n\n");

	return 0;
}
